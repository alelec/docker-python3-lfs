FROM python:3

RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash \
    && apt install -y git-lfs \
    && git-lfs install \
    && rm -rf /var/lib/apt/lists/*
